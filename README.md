# BDD Selenium Tests

## Prerequisites

- Python 3.7+
- PyCharm Professional
- Terminal

## Windows

- Open terminal (e.g. [cmder])
- Prepare virtual environment:

	venv-install.bat

- Install dependencies:

	pip install -r requirements.txt

- Import project to PyCharm

## macOS

- Open terminal
- Prepare virtual environment

	python -m venv venv
	source venv/bin/activate
	

- Install dependencies:

	pip install -r requirements.txt

- Import project to PyCharm
