from behave import *
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

base_url = "http://localhost:9998?language=en"
user_name = "test1"
user_email = "test1@icm"
user_password = "Test123!"


@given('icm app is working')
def step_impl(context):
    driver = context.driver
    driver.get(base_url)
    assert driver.find_element_by_link_text('ICM')


@when('user logs in')
def step_impl(context):
    driver = context.driver

    # Log out if user is already logged in
    logout_if_logged_in(driver)

    # Task 1: Put your code to log in here
    # Task 2: Put your code to check if user is successfully logged in here


@then('create incident button is visible')
def step_impl(context):
    driver = context.driver
    # Task 3 Put in here your code to check if create incident button is visible


def is_element_present(driver, how, what):
    try:
        driver.find_element(by=how, value=what)
    except NoSuchElementException:
        return False
    return True


def logout_if_logged_in(driver):
    if is_element_present(driver, By.LINK_TEXT, "Logout"):
        element = driver.find_element_by_xpath("//a[@href='/logout']")
        element.click()