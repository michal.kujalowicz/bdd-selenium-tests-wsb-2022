from tests import config
from tests import helpers


def before_all(context):

    # Setting up webdriver and adding it to the context
    driver = helpers.new_driver(driver="firefox", implicitly_wait=5)
    context.driver = driver


def after_all(context):
    driver = context.driver
    driver.close()




