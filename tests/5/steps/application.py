from behave import *

from tests import config

@given('icm app is working')
def step_impl(context):
    driver = context.driver
    driver.get(config.BASE_URL)
    assert driver.find_element_by_link_text('ICM')
